package com.ida.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GatewayController {

	@GetMapping("/")
	public String defaultGreeting() {
		String greeting = "<h1>Gateway</h1>";
		return greeting;
	}

	@RequestMapping("/timeout")
	public String timeout() throws InterruptedException {
		Thread.sleep(80000);
		return "timeout";
	}
}
